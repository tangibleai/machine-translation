# Create test set
import os
from pathlib import Path
import re
from zipfile import ZipFile
import unicodedata

import pandas as pd
import numpy as np

import torch
from torch.utils.data import Dataset

DATA_DIR = Path(Path(__file__).parent, '..', 'data')


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
from torch import FloatTensor, IntTensor, LongTensor  # noqa
if str(device) == 'gpu':
    from torch.cuda import FloatTensor, IntTensor, LongTensor  # noqa

print(f'torch.__version__: {torch.__version__}')
print(f'torch device: {device}')
print(f'IntTensor: {IntTensor}')
print(f'LongTensor: {LongTensor}')


# ## Dataset: Phrase pairs (2 languages)
#
# Download [manythings.org/anki/spa-eng.zip](http://www.manythings.org/anki/spa-eng.zip)

# In[34]:


dataset_path = Path(DATA_DIR, 'spa.txt')
if not dataset_path.exists():
    with ZipFile(Path(DATA_DIR, 'spa-eng.zip'), 'r') as zipobj:
        # Get a list of all archived file names from the zip
        # filenames = zipobj.namelist()
        zipobj.extract('spa.txt')

    # df = pd.read_csv(filename)


# In[35]:


lines = dataset_path.open(encoding='UTF-8').read().strip().split('\n')


# In[36]:

# source = input, target = output
# translating from input language to target language
source_lang_name = "spanish"
target_lang_name = "english"
SOS_TOKEN = '<start>'
EOS_TOKEN = '<stop>'
PAD_TOKEN = '<pad>'
OOV_TOKEN = '<oov>'
DUMMY_TOKENS = [SOS_TOKEN, EOS_TOKEN, PAD_TOKEN, OOV_TOKEN]

DEBUG = bool(os.environ.get('DEBUG'))
os.environ['DEBUG'] = str(DEBUG)

# Toy problem:
#    Overfit (memorize the training set) to get low loss, high accuracy
NUM_EXAMPLES = int(os.environ.get('NUM_EXAMPLES') or 2000)
os.environ['NUM_EXAMPLES'] = str(NUM_EXAMPLES)

# Hyperparam
NUM_EPOCHS = int(os.environ.get('NUM_EPOCHS') or 15)
os.environ['NUM_EPOCHS'] = str(NUM_EPOCHS)
LEARNING_RATE = float(os.environ.get('LEARNING_RATE') or 0.03)
os.environ['LEARNING_RATE'] = str(LEARNING_RATE)
BATCH_SIZE = int(os.environ.get('BATCH_SIZE') or 32)
os.environ['BATCH_SIZE'] = str(BATCH_SIZE)
EMBEDDING_DIM = int(os.environ.get('EMBEDDING_DIM') or 16)
LAYER_NUM_UNITS = eval(os.environ.get('LAYER_NUM_UNITS') or '[8]')

print(f"NUM_EPOCHS:{NUM_EPOCHS}  NUM_EXAMPLES:{NUM_EXAMPLES}  LEARNING_RATE:{LEARNING_RATE}")


# In[37]:

df = pd.read_csv(dataset_path, sep='\t', nrows=NUM_EXAMPLES, header=None, usecols=range(2))
df.columns = [target_lang_name, source_lang_name]
df.tail()


# In[38]:


def preprocess_sentence(s):
    """ Tokenize with simple multilingual tokenizer plus add <start> and <end> tokens

    Adds space between a word and the punctuation following it, so token
    >>> preprocess(" Hola!   ¿Que tal?   ")
    "Hola ! ¿ Que tal ?"

    Reference:
        https://stackoverflow.com/questions/3645931/python-padding-punctuation-with-white-spaces-keeping-punctuation
    """
    s = re.sub(r'([?.!,¿""\'])', r' \1 ', s)
    s = re.sub(r'[ ]+', ' ', s)
    # replace everything with space except (a-z, A-Z, "-", ".", "?", "!", ",")
    s = re.sub(r"[^-a-zA-Z?.!,¿]+", " ", s)
    s = s.strip()
    # adding a start and an end token to the sentence so RNN will work on variable length text
    return SOS_TOKEN + ' ' + s + ' ' + EOS_TOKEN


# In[39]:


for c in df.columns:
    df[c] = df[c].apply(lambda s: unicodedata.normalize('NFD', s))
    df[c] = df[c].apply(lambda s: preprocess_sentence(s))
print(df.sample(5))


# In[55]:


class LanguageIndex():
    """ Create vocabulary mapping and index (inverse mapping)

    >>> langindex = LanguageIndex(df['english'], name='English')
    >>> list(langindex.word2idx.items())[:8]
    [('!', 0),
     (',', 1),
     ('.', 2),
     ('<oov>', 3),
     ('<pad>', 4),
     ('<start>', 5),
     ('<stop>', 6),
     ('?', 7)]
    >>> langindex[3]
    'oov'
    >>> langindex['?']
    7
    """

    def __init__(self, phrases, name=None):
        """ `phrases` is a list of phrases in one language """
        self.name = name  # 'english', 'spanish', etc
        self.word2idx = {}
        self.vocab = []
        self.size = 0
        self.idx2word = self.vocab  # this can just be a list
        self.max_phrase_length = 0
        self.create_index(phrases)

    def create_index(self, phrases):
        self.vocab = set(DUMMY_TOKENS)
        for phrase in phrases:
            tokens = phrase.split()
            self.max_phrase_length = max(self.max_phrase_length, len(tokens))
            self.vocab.update(set(tokens))
        self.vocab = sorted(self.vocab)

        self.idx2word = self.vocab
        self.size = len(self.idx2word)
        self.word2idx = dict(zip(self.vocab, range(len(self.vocab))))

    def get(self, tok, default=None):
        # If a new vocab is not found, will return token of 'oov'
        if isinstance(tok, int):
            if (0 <= tok < self.size):
                return self.idx2word[tok]
            return None
        return self.word2idx.get(tok, self.word2idx['<oov>'])

    def __getitem__(self, tok):
        if isinstance(tok, int):
            return self.idx2word[tok]
        return self.word2idx[tok]


# In[57]:

def max_length(tensor):
    return max(len(t) for t in tensor)


def pad_token_id_list(s, max_len, pad_tok_idx):
    """ Add the padding token id to the end of the list of integers to ensure uniform length

    TODO: make this a method within the LanguageIndex so the pad_tok_idx is known within self

    Input:
      max_len (int): maximum number of tokens in a sentence
      pad_tok_idx (int): the id of the token '<pad>' for the sentence (language) being padded
    Output:
      sequence of ints with the integer pad token appended to the end
    """
    padded = pad_tok_idx * np.ones(max_len, dtype=np.int64)  # FIXME: int16 should be plenty
    s_len = min(max_len, len(s))
    padded[:s_len] = s[:s_len]
    return padded


# In[66]:

def custom_train_test_split(df, source_lang_name=None, target_lang_name=None):
    target_lang_name = target_lang_name or df.columns[0]
    source_lang_name = source_lang_name or df.columns[1]
    print("original df shape :{}".format(df.shape))
    df['check_duplicates_source'] = df[source_lang_name].duplicated()
    df = df[~df['check_duplicates_source']]
    df['check_duplicates_target'] = df[target_lang_name].duplicated()
    df = df[~df['check_duplicates_target']]
    print("df shape after removing duplicate:{}".format(df.shape))

    num_train_examples = int(len(df) * 0.95)
    train_df = df[:num_train_examples]
    val_df = df[num_train_examples:]
    val_df['check_duplicates_input'] = val_df[source_lang_name].isin(list(train_df[source_lang_name]))
    val_df['check_duplicates_target'] = val_df[target_lang_name].isin(list(train_df[source_lang_name]))

    cond1 = ~val_df['check_duplicates_input']
    cond2 = ~val_df['check_duplicates_target']
    val_df = val_df[cond1 & cond2]
    print("after checking against training data", val_df.shape)

    return train_df, val_df


def remove_duplicates(sent, string_list):
    """ Check a string exists in a column of a dataframe"""
    if sent in string_list:
        return True
    else:
        return False


# Split training and val set and remove overlapping sentences from val
train_df, val_df = custom_train_test_split(df, source_lang_name, target_lang_name)

# Create token dictionary
input_word_index = LanguageIndex(phrases=train_df[source_lang_name].values, name=source_lang_name)
target_word_index = LanguageIndex(phrases=train_df[target_lang_name].values, name=target_lang_name)

# Vectorize the input and target languages
input_train_tensors = [[input_word_index.get(s) for s in es.split(' ')] for es in train_df[source_lang_name].values.tolist()]
input_val_tensors = [[input_word_index.get(s) for s in es.split(' ')] for es in val_df[source_lang_name].values.tolist()]

target_train_tensors = [[target_word_index.get(s) for s in eng.split(' ')] for eng in train_df[target_lang_name].values.tolist()]
target_val_tensors = [[target_word_index.get(s) for s in eng.split(' ')] for eng in val_df[target_lang_name].values.tolist()]

# calculate the max_length of input and output tensor
max_length_inp_train = max(len(t) for t in input_train_tensors)
max_length_inp_val = max(len(t) for t in input_val_tensors)
max_length_inp = max(max_length_inp_train, max_length_inp_val)
max_length_tar_train = max(len(t) for t in target_train_tensors)
max_length_tar_val = max(len(t) for t in target_val_tensors)
max_length_tar = max(max_length_tar_train, max_length_tar_val)
max_length_inp, max_length_tar

# inplace padding
input_train_tensors = [pad_token_id_list(x, max_length_inp, input_word_index[PAD_TOKEN]) for x in input_train_tensors]
target_train_tensors = [pad_token_id_list(x, max_length_tar, target_word_index[PAD_TOKEN]) for x in target_train_tensors]
input_val_tensors = [pad_token_id_list(x, max_length_inp, input_word_index[PAD_TOKEN]) for x in input_val_tensors]
target_val_tensors = [pad_token_id_list(x, max_length_tar, target_word_index[PAD_TOKEN]) for x in target_val_tensors]
print(f"len(input_train_tensors): {len(input_train_tensors)}")


# In[67]:
def remove_sentence_with_unseen_token(input_val_tensors, target_val_tensors, input_word_index, target_word_index):

    input_tensor_val_filtered = []
    target_tensor_val_filtered = []

    for input_sent, target_sent in zip(input_val_tensors, target_val_tensors):
        if input_oov_token not in input_sent and target_oov_token not in target_sent:
            input_tensor_val_filtered.append(input_sent)
            target_tensor_val_filtered.append(target_sent)

    return input_tensor_val_filtered, target_tensor_val_filtered


# Show length
print("show length")
print(len(input_train_tensors), len(target_train_tensors), len(input_val_tensors), len(target_val_tensors))

input_oov_token = input_word_index.get('<oov>')
target_oov_token = target_word_index.get('<oov>')

input_tensor_val_filtered, target_tensor_val_filtered = remove_sentence_with_unseen_token(
    input_val_tensors, target_val_tensors, input_word_index, target_word_index)
# Check 1: only using vocabulary in training set
"""
input_tensor_val_filtered = []
target_tensor_val_filtered = []

for input_sent, target_sent in zip(input_val_tensors,target_val_tensors):
    if input_oov_token not in input_sent and target_oov_token not in target_sent:
        input_tensor_val_filtered.append(input_sent)
        target_tensor_val_filtered.append(target_sent)
"""

print("Remove sent with oov tokens")
print(len(input_train_tensors), len(target_train_tensors), len(input_tensor_val_filtered), len(target_tensor_val_filtered))


print(target_tensor_val_filtered[:5])

# In[68]:


# In[69]:


class TranslationDataset(Dataset):
    """ Convert each vector to torch.tensor type and wrap with Dataloader() """

    def __init__(self, X, y):
        """ Dataset of phrase pairs represented as arrays of integers """
        self.data = X
        self.target = y
        # FIXME: vectorize with torch.tensor
        self.length = [np.sum(1 - np.equal(x, 0)) for x in X]

    def __getitem__(self, index):
        x = self.data[index]
        y = self.target[index]
        x_len = self.length[index]
        return x, y, x_len

    def __len__(self):
        return len(self.data)


# In[70]:


train_dataset = TranslationDataset(input_train_tensors, target_train_tensors)
val_dataset = TranslationDataset(input_tensor_val_filtered, target_tensor_val_filtered)
