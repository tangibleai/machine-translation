""" Neural Machine Translation global constants """
import os
from pathlib import Path

DATA_DIR = Path(__file__).parent.parent.parent / 'data'

SOS_TOKEN = '<start>'
EOS_TOKEN = '<stop>'
PAD_TOKEN = '<pad>'
OOV_TOKEN = '<oov>'
DUMMY_TOKENS = [SOS_TOKEN, EOS_TOKEN, PAD_TOKEN, OOV_TOKEN]

NUM_EXAMPLES = int(os.environ.get('NUM_EXAMPLES') or 128)

# Hyperparams
NUM_EPOCHS = int(os.environ.get('NUM_EPOCHS') or 2)
LEARNING_RATE = float(os.environ.get('LEARNING_RATE') or 0.03)
BATCH_SIZE = int(os.environ.get('BATCH_SIZE') or 32)
EMBEDDING_DIM = int(os.environ.get('EMBEDDING_DIM') or 20)
LAYER_NUM_UNITS = eval(os.environ.get('LAYER_NUM_UNITS') or '[32]')
