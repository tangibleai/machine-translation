# encoders.py
""" Pytorch RNN encoder, decoder, and seq2seq Models

* RNN
* LSTM
* GRU
* GRUAttentionEncoder, GRUAttentionEncoder
"""
import torch
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.autograd import Variable
from torch import FloatTensor, IntTensor, LongTensor  # noqa

from nmt.utils.preprocess import sort_batch
from nmt.models.helpers import mask_3d
from nmt.constants import EMBEDDING_DIM, LAYER_NUM_UNITS, BATCH_SIZE


class Encoder(nn.Module):
    """ TODO: Hobson (modularize) """

    def __init__(self,
                 vocab_size,
                 embedding_dim=EMBEDDING_DIM,
                 layer_enc_units=LAYER_NUM_UNITS,
                 batch_size=BATCH_SIZE,
                 device=None):
        super().__init__()
        self.batch_size = batch_size
        self.layer_enc_units = layer_enc_units
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        if device is None:
            if torch.cuda.is_available():
                device = torch.device("cuda")
            else:
                device = torch.device("cpu")
        self.device = device
        self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.gru_layers = []
        for enc_units in self.layer_enc_units:
            self.gru_layers.append(nn.GRU(self.embedding_dim, enc_units))
            self.gru_layers[-1].to(device)

    def forward(self, X, lengths=None):

        lengths = LongTensor([len(s)
                              for s in X]) if lengths is None else lengths
        # x: batch_size, max_length

        # x: batch_size, max_length, embedding_dim
        X = self.embedding(X)

        # x transformed = max_len X batch_size X embedding_dim
        # x = x.permute(1,0,2)
        self.output = pack_padded_sequence(X, lengths)  # unpad

        self.hidden = self.initialize_hidden_state()

        # output: max_length, batch_size, enc_units
        # self.hidden: 1, batch_size, enc_units
        # gru returns hidden state of all timesteps as well as hidden state at last timestep
        for gru in self.gru_layers:
            self.output, self.hidden = gru(self.output, self.hidden)

        # pad the sequence to the max length in the batch
        self.output, _ = pad_packed_sequence(self.output)

        return self.output, self.hidden

    def initialize_hidden_state(self):
        return torch.zeros((1, self.batch_size, self.layer_enc_units[-1])).to(self.device)


class Decoder(nn.Module):
    def __init__(self,
                 vocab_size,
                 embedding_dim=EMBEDDING_DIM,
                 layer_enc_units=LAYER_NUM_UNITS,
                 dec_units=LAYER_NUM_UNITS[-1],
                 batch_size=BATCH_SIZE):
        super(Decoder, self).__init__()
        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.enc_units = layer_enc_units[-1]
        self.dec_units = dec_units
        self.batch_size = batch_size

        self.embedding = nn.Embedding(self.vocab_size, self.embedding_dim)
        self.gru = nn.GRU(self.embedding_dim + self.enc_units,
                          self.dec_units,
                          batch_first=True)
        self.fc = nn.Linear(self.enc_units, self.vocab_size)

        # used for attention
        self.W1 = nn.Linear(self.enc_units, self.dec_units)
        self.W2 = nn.Linear(self.enc_units, self.dec_units)
        self.V = nn.Linear(self.enc_units, 1)

    def forward(self, X, hidden, enc_output):
        # enc_output original: (max_length, batch_size, enc_units)
        # enc_output converted == (batch_size, max_length, hidden_size)
        enc_output = enc_output.permute(1, 0, 2)
        # hidden shape == (batch_size, hidden size)
        # hidden_with_time_axis shape == (batch_size, 1, hidden size)
        # we are doing this to perform addition to calculate the score

        # hidden shape == (batch_size, hidden size)
        # hidden_with_time_axis shape == (batch_size, 1, hidden size)
        hidden_with_time_axis = hidden.permute(1, 0, 2)

        # score: (batch_size, max_length, hidden_size) # Bahdanaus's
        # we get 1 at the last axis because we are applying tanh(FC(EO) + FC(H)) to self.V
        # It doesn't matter which FC we pick for each of the inputs
        score = torch.tanh(self.W1(enc_output) +
                           self.W2(hidden_with_time_axis))

        # score = torch.tanh(self.W2(hidden_with_time_axis) + self.W1(enc_output))

        # attention_weights shape == (batch_size, max_length, 1)
        # we get 1 at the last axis because we are applying score to self.V
        attention_weights = torch.softmax(self.V(score), dim=1)

        # context_vector shape after sum == (batch_size, hidden_size)
        context_vector = attention_weights * enc_output
        context_vector = torch.sum(context_vector, dim=1)

        # x shape after passing through embedding == (batch_size, 1, embedding_dim)
        # takes case of the right portion of the model above (illustrated in red)
        X = self.embedding(X)

        # x shape after concatenation == (batch_size, 1, embedding_dim + hidden_size)
        # x = tf.concat([tf.expand_dims(context_vector, 1), x], axis=-1)
        # ? Looks like attention vector in diagram of source
        X = torch.cat((context_vector.unsqueeze(1), X), -1)

        # passing the concatenated vector to the GRU
        # output: (batch_size, 1, hidden_size)
        output, state = self.gru(X)

        # output shape == (batch_size * 1, hidden_size)
        output = output.view(-1, output.size(2))

        # output shape == (batch_size * 1, vocab)
        X = self.fc(output)

        return X, state, attention_weights

    def initialize_hidden_state(self):
        return torch.zeros((1, self.batch_sz, self.dec_units))


class Seq2Seq(nn.Module):
    """
        Sequence to sequence module
    """

    def __init__(self, config, vocab_inp_size, vocab_out_size):
        super(Seq2Seq, self).__init__()
        self.SOS = config.get("vocab", {}).get("start_token", 1)
        self.EOS = config.get("vocab", {}).get("end_token", 2)
        self.vocab_inp_size = vocab_inp_size
        self.vocab_out_size = vocab_out_size
        self.batch_size = config.get("batch_size", 64)
        self.gpu = config.get("gpu", False)
        self.debug = config.get("debug", False)
        self.training = False
        self.device = torch.device("cuda" if self.gpu else "cpu")
        self.target_max_length = 16

        if config.get('loss') == 'cross_entropy':
            self.loss_fn = torch.nn.CrossEntropyLoss(ignore_index=0)

        # Encoder
        self.encoder = Encoder(config, vocab_inp_size)

        # Decoder
        self.decoder = Decoder(config, vocab_out_size)

        # Loss Function
        self.loss_fn = torch.nn.CrossEntropyLoss(ignore_index=0)

        print(config)

    def encode(self, x, x_len):
        """
        Given Input Sequence, Pass the Data to Encode

        Return:
        - Encoder Output
        - Encoder State
        """

        # Check to see if batch_size parameter is fixed or base on input batch
        cur_batch_size = x.size()[1]
        encode_init_state = self.encoder.initialize_hidden_state(cur_batch_size)
        encoder_state, encoder_outputs = self.encoder.forward(
            x.to(self.device),
            encode_init_state.to(self.device),
            x_len.to(self.device))

        return encoder_outputs, encoder_state

    def decode(self, encoder_outputs, encoder_hidden, targets, targets_lengths):
        """
        In the simplest seq2seq decoder we use only last
        output of the encoder. This last output is sometimes
        called the context vector as it encodes context
        from the entire sequence. This context vector is used
        as the initial hidden state of the decoder.

        At every step of decoding, the decoder is given an input
        token and hidden state. The initial input token
        is the start-of-string <SOS> token, and the first
        hidden state is the context vector
        (the encoder’s last hidden state).

        Args:
            encoder_outputs: (B, T, H)
            encoder_hidden: (B, H)
            targets: (B, L)
            targets_lengths: (B)
            input_lengths: (B)
        Vars:
            decoder_input: (B)
            decoder_context: (B, H)
            hidden_state: (B, H)
            attention_weights: (B, T)
        Outputs:
            alignments: (L, T, B)
            logits: (B*L, V)
            labels: (B*L)
        """
        batch_size = encoder_outputs.size()[1]
        max_length = targets.size()[1]
        decoder_input = torch.tensor([[self.SOS]] * batch_size)
        decoder_hidden = encoder_outputs

        logits = Variable(torch.zeros(max_length, batch_size, self.decoder.vocab_size))
        final_sentences = Variable(torch.zeros(batch_size, max_length))

        if self.debug:
            print("decoder_input dim: {}".format(decoder_input.shape))
            print("decoder hidden dim:{}".format(decoder_hidden.shape))
            print("encoder output dim:{}".format(encoder_outputs.shape))
            print("targets dim: {}".format(targets.shape))
            print("targets_length: {}".format(targets_lengths.shape))
            print("final sentences:")
            print(final_sentences)

        if self.gpu:
            decoder_input = decoder_input.cuda()
            decoder_hidden = decoder_hidden.cuda()
            logits = logits.cuda()
            final_sentences = final_sentences.cuda()

        for t in range(1, max_length):
            # The decoder accepts, at each time step t :
            # - an input, [B]
            # - a context, [B, H]
            # - an hidden state, [B, H]
            # - encoder outputs, [B, T, H]

            # The decoder outputs, at each time step t :
            # - an output, [B]
            # - a context, [B, H]
            # - an hidden state, [B, H]
            # - weights, [B, T]

            # enc_hidden: 1, batch_size, enc_units
            # output: max_length, batch_size, enc_units
            predictions, decoder_hidden = self.decoder.forward(decoder_input.to(self.device),
                                                               decoder_hidden.to(self.device))

            # Store Prediction at time step t
            logits[t] = predictions

            if self.training:
                decoder_input = targets[:, t].unsqueeze(1)
            else:
                decoder_input = torch.argmax(predictions, axis=1).unsqueeze(1)
                final_sentences[:, t] = decoder_input.squeeze(1)

                if self.debug:
                    print("final sentences:")
                    print(final_sentences)
        labels = targets.contiguous().view(-1)
        mask_value = 0

        # Masking the logits to prepare for eval
        logits = mask_3d(logits.transpose(1, 0), targets_lengths, mask_value)
        logits = logits.contiguous().view(-1, self.vocab_out_size)
        if self.debug:
            print("Logit dimension: {}".format(logits.shape))
            print("Label dimension: {}".format(labels.shape))
        # Return final state, labels
        return logits, labels.long(), final_sentences

    def step(self, batch, sorted=False):

        x, y, x_len, y_len = batch

        if not sorted:
            x_sorted, y_sorted, x_len_sorted, y_len_sorted = sort_batch(x, y, x_len, y_len)
        else:
            x_sorted, y_sorted, x_len_sorted, y_len_sorted = x, y, x_len, y_len

        if self.debug:
            print("x_sorted: {}".format(x_sorted.shape))
            print("y_sorted: {}".format(y_sorted.shape))
            print("x_len_sorted: {}".format(x_len_sorted.shape))
            print("y_len_sorted: {}".format(y_len_sorted.shape))
        if self.gpu:
            x_sorted = x_sorted.cuda()
            y_sorted = y_sorted.cuda()
            x_len_sorted = x_len_sorted.cuda()
            y_len_sorted = y_len_sorted.cuda()

        encoder_out, encoder_state = self.encode(x_sorted, x_len_sorted)

        if self.debug:
            if encoder_out.size()[1] > 1:
                a = encoder_out[:, 0, :]
                b = encoder_out[:, 1, :]
                print("check equal tensor 0,1 == {}".format(torch.all(torch.eq(a, b))))

        logits, labels, final_sentences = self.decode(encoder_out, encoder_state, y_sorted, y_len_sorted)
        return logits, labels, final_sentences

    def loss(self, batch, sorted=False):
        logits, labels, final_sentences = self.step(batch, sorted=sorted)
        loss = self.loss_fn(logits, labels)
        return loss, logits, labels, final_sentences
