import unicodedata
import re
import numpy as np

from nmt.constants import EOS_TOKEN, SOS_TOKEN  # OOV_TOKEN

# Converts the unicode file to ascii


def preprocess_sentence(s):
    r""" Tokenize with simple multilingual tokenizer plus add <start> and <stop> tokens

    Adds space between a word and the punctuation following it, so token
    >>> preprocess_sentence(" Hola!   ¿Que tal?   ")
    '<start> Hola ! ¿ Que tal ? <stop>'

    Reference:
        https://stackoverflow.com/questions/3645931/python-padding-punctuation-with-white-spaces-keeping-punctuation
    """
    s = re.sub(r'([?.!,¿""\'])', r' \1 ', s)
    s = re.sub(r'[ ]+', ' ', s)
    # replace everything with space except (a-z, A-Z, "-", ".", "?", "!", ",")
    s = re.sub(r"[^-a-zA-Z?.!,¿]+", " ", s)
    s = s.strip()
    # adding a start and an end token to the sentence so RNN will work on variable length text
    return SOS_TOKEN + ' ' + s + ' ' + EOS_TOKEN


def unicode_to_ascii(s):
    """ Normalizes latin chars with accent to their canonical decomposition """
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


def max_length(tensor):
    """ TODO: this is where yours goes Hanna"""
    return max(len(t) for t in tensor)


def pad_sequences(x, max_len):
    padded = np.zeros((max_len), dtype=np.int64)
    if len(x) > max_len:
        padded[:] = x[:max_len]
    else:
        padded[:len(x)] = x
    return padded


def pad_token_id_list(s, max_len, pad_tok_idx):
    """ Add the padding token id to the end of the list of integers to ensure uniform length

    TODO: make this a method within the LanguageIndex so the pad_tok_idx is known within self

    Input:
      max_len (int): maximum number of tokens in a sentence
      pad_tok_idx (int): the id of the token '<pad>' for the sentence (language) being padded
    Output:
      sequence of ints with the integer pad token appended to the end
    """
    padded = pad_tok_idx * np.ones(max_len, dtype=np.int64)  # FIXME: int16 should be plenty
    s_len = min(max_len, len(s))
    try:
        padded[:s_len] = s[:s_len]
    except:
        print("problem child:-- ",s,s_len)
    return padded

# sort batch function to be able to use with pad_packed_sequence
# Can be use for bucketing later to reduce time complexity while training

# TODO: Sylvia put convert_to_sentence(s) here

def sort_batch(X, y, x_length, y_length):
    x_length, indx = x_length.sort(dim=0, descending=True)
    X = X[indx]
    y = y[indx]
    y_length = y_length[indx]
    return X.transpose(0, 1), y, x_length, y_length  # transpose (batch x seq) to (seq x batch)


if __name__ == '__main__':
    sentence = "I am a man?"
    print(preprocess_sentence(sentence).split())

    phrase = [2, 3, 4, 5]
    print(pad_sequences(phrase, max_len=16))
