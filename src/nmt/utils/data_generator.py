from torch.utils.data import Dataset
import numpy as np
from collections import Counter
from scipy.stats import entropy

SOS_TOKEN = '<start>'
EOS_TOKEN = '<stop>'
PAD_TOKEN = '<pad>'
OOV_TOKEN = '<oov>'
DUMMY_TOKENS = [SOS_TOKEN, EOS_TOKEN, PAD_TOKEN, OOV_TOKEN]


class TranslationDataset(Dataset):
    """ Convert the data to tensors for use in training loop as a torch.Dataset iterable

    >>> X = [[1,2,3,5,0],[2,3,49,0],[9,3,4,592,0]]
    >>> y = [[2,3,5,0,77],[3,49,0,77],[3,4,592,0,77]]
    >>> test_DS = TranslationDataset(X,y)
    >>> X_test, y_test, x_len_test = test_DS.__getitem__(1)
    >>> X_test
    [2, 3, 49, 0]
    >>> y_test
    [3, 49, 0, 77]
    """

    def __init__(self, X, y):
        self.data = X
        self.target = y
        self.length = [np.sum(1 - np.equal(x, 0)) for x in X]

    def __getitem__(self, index):
        x = self.data[index]
        y = self.target[index]
        x_len = self.length[index]
        return x, y, x_len

    def __len__(self):
        return len(self.data)


class LanguageIndex():
    """
    # This class creates a word -> index mapping (e.g,. "dad" -> 5) and vice-versa
    # (e.g., 5 -> "dad") for each language,
    """

    def __init__(self, phrases, name=None, **kwargs):
        """ `phrases` is a list of phrases in one language """
        self.name = name  # 'english', 'spanish', etc
        self.word2idx = {}
        self.vocab = []
        self.idx2word = self.vocab  # this can just be a list
        self.max_phrase_length = 0
        self.entropy = None
        self.word_frequency_counter = Counter()
        self.create_index(phrases)
        self.calculate_entropy()

    def create_index(self, phrases):
        self.vocab = set(DUMMY_TOKENS)
        for phrase in phrases:
            tokens = phrase.split()
            # print(tokens)
            self.max_phrase_length = max(self.max_phrase_length, len(tokens))
            self.vocab.update(set(tokens))
            self.word_frequency_counter.update(tokens)
        self.vocab = sorted(self.vocab)

        self.idx2word = self.vocab
        self.word2idx = dict(zip(self.vocab, range(len(self.vocab))))

    def calculate_entropy(self, base=2):
        self.entropy = self.entropy or entropy(list(self.word_frequency_counter.values()), base=base)
        return entropy

    def get(self, tok, default=None):
        # If not found, return oov token
        if isinstance(tok, int):
            if (0 <= tok < len(self)):
                return self.idx2word[tok]
            return None
        return self.word2idx.get(tok, self.word2idx['<oov>'])

    def __getitem__(self, tok):
        if isinstance(tok, int):
            return self.idx2word[tok]
        return self.word2idx[tok]

    def __len__(self):
        return len(self.vocab)


def custom_train_test_split(df, source_lang_name, target_lang_name, train_size=None, test_size=.95, remove_dupes=False):
    if remove_dupes:
        print("original df shape :{}".format(df.shape))
        df['check_duplicates_source'] = df[source_lang_name].duplicated()
        df = df[~df['check_duplicates_source']].copy()
        df['check_duplicates_target'] = df[target_lang_name].duplicated()
        df = df[~df['check_duplicates_target']].copy()
        print("df shape after removing duplicate:{}".format(df.shape))

    if isinstance(train_size, int):
        num_train_examples = train_size
    else:
        num_train_examples = int(len(df) * (train_size or (1 - test_size)))
    train_df = df[:num_train_examples]
    val_df = df[num_train_examples:]
    source_val_example_in_trainset = val_df[source_lang_name].isin(set(train_df[source_lang_name]))
    target_val_example_in_trainset = val_df[target_lang_name].isin(set(train_df[target_lang_name]))

    val_df = val_df[~source_val_example_in_trainset & ~target_val_example_in_trainset]
    return train_df, val_df


def remove_sentence_with_unseen_token(input_val_tensors, target_val_tensors, input_word_index, target_word_index):
    input_oov_token = input_word_index.get('<oov>')
    target_oov_token = target_word_index.get('<oov>')

    input_tensor_val_filtered = []
    target_tensor_val_filtered = []

    for input_sent, target_sent in zip(input_val_tensors, target_val_tensors):
        if input_oov_token not in input_sent and target_oov_token not in target_sent:
            input_tensor_val_filtered.append(input_sent)
            target_tensor_val_filtered.append(target_sent)

    return input_tensor_val_filtered, target_tensor_val_filtered


if __name__ == '__main__':
    phrases = ['<start> i am a man <stop>', '<start> weather is great <stop>']
    test_language_index = LanguageIndex(phrases)
    print(test_language_index.word2idx)
    print(test_language_index.idx2word)
    print(test_language_index.get("ambacadabra"))
