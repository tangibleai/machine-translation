#!/usr/bin/env python
""" Neural Machine Translation """
# [GitHub version](https://github.com/omarsar/pytorch_neural_machine_translation_attention) of this notebook.
# I will appreciate it!
#
# We didn't evaluate the model or analyzed it.
# To encourage you to practice what you have learned in the notebook,
# I will suggest that you try to convert the TensorFlow code used in the
#  [original notebook]
# (https://colab.research.google.com/github/tensorflow/tensorflow/blob/master/tensorflow/contrib/eager/python/examples/nmt_with_attention/nmt_with_attention.ipynb)
# and complete this notebook. I believe the code should be straightforward,
# the hard part was already done in this notebook. If you manage to complete it, please submit a PR on the GitHub
# version of this notebook.
# I will gladly accept your PR.
# Thanks for reading and hope this notebook was useful. Keep tuned for notebooks like this on my Twitter
# ([omarsar0](https://twitter.com/omarsar0)).

# ## References
#
# ### Seq2Seq:
#   - Sutskever et al. (2014)
#     - [Sequence to Sequence Learning with Neural Networks](Sequence to Sequence Learning with Neural Networks)
#   - [Sequence to sequence model: Introduction and concepts]
# (https://towardsdatascience.com/sequence-to-sequence-model-introduction-and-concepts-44d9b41cd42d)
#   - [Blog on seq2seq](https://guillaumegenthial.github.io/sequence-to-sequence.html)
#   - [Bahdanau et al. (2016) NMT jointly learning to align and translate](https://arxiv.org/pdf/1409.0473.pdf)
#   - [Attention is all you need](https://arxiv.org/pdf/1706.03762.pdf)

# !/usr/bin/env python
# coding: utf-8
import os
from pathlib import Path
import time
from zipfile import ZipFile
import unicodedata

from tqdm import tqdm
import pandas as pd
# from sklearn.model_selection import train_test_split

import torch
from torch import nn
from torch import optim
from torch.utils.data import DataLoader
from torch.autograd import Variable

from nmt.constants import SOS_TOKEN, PAD_TOKEN, DUMMY_TOKENS, DATA_DIR  # EOS_TOKEN, OOV_TOKEN
from nmt.constants import NUM_EXAMPLES, NUM_EPOCHS, LEARNING_RATE, BATCH_SIZE, EMBEDDING_DIM, LAYER_NUM_UNITS
from nmt.utils.data_generator import LanguageIndex, TranslationDataset, custom_train_test_split
from nmt.utils.load_raw_data import texts_to_tensors
from nmt.utils.preprocess import pad_token_id_list
from nmt.utils.token_intersection import rouge_n
from nmt.utils.preprocess import preprocess_sentence
from nmt.models.seq2seq import Encoder, Decoder


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

if str(DEVICE) == 'cuda':
    from torch.cuda import FloatTensor, IntTensor, LongTensor  # noqa
else:
    from torch import FloatTensor, IntTensor, LongTensor  # noqa

print(f'torch.__version__: {torch.__version__}')
print(f'torch DEVICE: {DEVICE}')
print(f'LongTensor: {FloatTensor}')
print(f'IntTensor: {IntTensor}')
print(f'LongTensor: {LongTensor}')


dataset_path = Path(DATA_DIR, 'spa.txt')
if not dataset_path.exists():
    with ZipFile(Path(DATA_DIR, 'spa-eng.zip'), 'r') as zipobj:
        # Get a list of all archived file names from the zip
        # filenames = zipobj.namelist()
        zipobj.extract('spa.txt')


lines = dataset_path.open(encoding='UTF-8').read().strip().split('\n')


# In[36]:

# source = input, target = output
# translating from input language to target language
source_lang_name = "spanish"
target_lang_name = "english"

DEBUG = bool(os.environ.get('DEBUG'))
os.environ['DEBUG'] = str(DEBUG)


print(f"NUM_EPOCHS:{NUM_EPOCHS}\n"
      f"NUM_EXAMPLES:{NUM_EXAMPLES}\n"
      f"LEARNING_RATE:{LEARNING_RATE}\n"
      )


def load_dataset(nrows=NUM_EXAMPLES, usecols=range(2)):
    # if we reversed the order of usecols would it reverse them in the dataframe so we could change direction of translation?
    df = pd.read_csv(dataset_path, sep='\t', nrows=nrows,
                     header=None, usecols=usecols)
    df.columns = [target_lang_name, source_lang_name]
    # df.tail()

    for c in df.columns:
        df[c] = df[c].apply(lambda s: unicodedata.normalize('NFD', s))
        df[c] = df[c].apply(lambda s: preprocess_sentence(s))

    return df


def max_length(tensor):
    """ TODO Hanna """
    return max(len(t) for t in tensor)


def sort_batch(X, y, lengths, descending_length=True):
    """ Sort batch function to be able to use with pad_packed_sequence """
    lengths, idx = lengths.sort(
        dim=0, descending=descending_length)  # this means
    X = X[idx]
    y = y[idx]
    # transpose (batch x seq) to (seq x batch)
    return X.transpose(0, 1), y, lengths


criterion = nn.CrossEntropyLoss()


def loss_function(real, pred):
    """ Only consider non-zero inputs in the loss; mask needed """
    # mask = 1 - np.equal(real, 0) # assign 0 to all above 0 and 1 to all 0s
    # print(mask)
    mask = real.ge(1).type(FloatTensor)

    loss_ = criterion(pred, real) * mask
    return torch.mean(loss_)


# - Pass the input through the encoder which return encoder output and the encoder hidden state.
# - The encoder output, encoder hidden state and the decoder input (which is the start token) is passed to the decoder.
# - The decoder returns the predictions and the decoder hidden state.
# - The decoder hidden state is then passed back into the model and the predictions are used to calculate the loss.
# - Use teacher forcing to decide the next input to the decoder.
# - Teacher forcing is the technique where the target word is passed as the next input to the decoder.
# - The final step is to calculate the gradients and apply it to the optimizer and backpropagate.


def train(encoder, decoder, optimizer, dataset, num_epochs=NUM_EPOCHS, device=DEVICE):
    for epoch in tqdm(range(num_epochs)):
        start = time.time()

        encoder.train()
        decoder.train()

        total_loss = 0

        for (batch, (inp, targ, inp_len)) in enumerate(dataset):
            loss = 0

            xs, ys, lengths = sort_batch(X=inp, y=targ, lengths=inp_len)
            enc_output, enc_hidden = encoder(
                xs.to(DEVICE), lengths)
            dec_hidden = enc_hidden

            # use teacher forcing - feeding the target as the next input (via dec_input)
            dec_input = torch.tensor(
                [[target_word_index.word2idx[SOS_TOKEN]]] * BATCH_SIZE)

            # run code below for every timestep in the ys batch
            for t in range(1, ys.size(1)):
                predictions, dec_hidden, _ = decoder(dec_input.to(device),
                                                     dec_hidden.to(device),
                                                     enc_output.to(device))
                loss += loss_function(ys[:, t].to(device),
                                      predictions.to(device))
                # loss += loss_
                dec_input = ys[:, t].unsqueeze(1)

            batch_loss = (loss / int(ys.size(1)))
            total_loss += batch_loss

            optimizer.zero_grad()

            loss.backward()

            # UPDATE MODEL PARAMETERS
            optimizer.step()

        # print('Epoch {} Loss {:.4f}'.format(batch, batch_loss.detach().item()))

        # TODO: Save checkpoint for model
        print(
            f"Epoch {(epoch+1):03d} Loss {total_loss / N_BATCH:.2f}  Time:{(time.time()-start):03.3f} s")
    return encoder, decoder


def convert_to_sentence(int_seq, word_index):
    """ convert tensor of integers to human readable sentence

    >>> li = LanguageIndex(['I like ice cream .'])
    >>> convert_to_sentence(IntTensor([5,8,7,6,0]), li)
    'I like ice cream .'

    """
    token_seq = [word_index[int(tok.item())] for tok in int_seq]
    token_seq = [tok for tok in token_seq if tok not in DUMMY_TOKENS]
    return ' '.join(token_seq)


def convert_to_sentences(batch, word_index):
    """ convert a batch of tensor integers to human readable sentences

    >>> lis = LanguageIndex(['Have a nice day !', 'I like playing tennis .'])
    >>> convert_to_sentences([IntTensor([6,8,9,0]), IntTensor([7,10,12,13,1])], lis)
    ['Have a day !', 'I like playing tennis .']

    """
    sentences = []
    for int_seq in batch:
        sentences.append(convert_to_sentence(int_seq, word_index=word_index))
    return sentences

# In[ ]:


def predict_batch(dataset, num_batches, source_word_index, target_word_index, device=DEVICE):
    """ Calculates total (average) loss and returns translated sentences, correct translations (truth) etc """
    target_sentences = []
    translated_sentences = []
    input_sentences = []
    total_loss = 0
    batch_loss = 0
    for (batch_num, (inp, targ, inp_len)) in enumerate(dataset):
        if batch_num >= num_batches:
            break
        loss = 0

        # FIXME: don't sort
        Xs, ys, lengths = sort_batch(X=inp, y=targ, lengths=inp_len)
        enc_output, enc_hidden = encoder(Xs.to(device), lengths)
        dec_hidden = enc_hidden

        final_sentences = Variable(torch.zeros(ys.size()))

        # winnie:   ( use teacher forcing - feeding the target as the next input (via dec_input))
        # dec_input = torch.tensor([target_word_index.word2idx[SOS_TOKEN]] * BATCH_SIZE)

        # original: ( use teacher forcing - feeding the target as the next input (via dec_input))
        dec_input = torch.tensor(
            [[target_word_index.word2idx[SOS_TOKEN]]] * BATCH_SIZE)
        final_sentences[:, 0] = LongTensor(
            [target_word_index.word2idx[SOS_TOKEN]] * BATCH_SIZE)

        # run code below for every timestep in the ys for this batch
        for t in range(1, ys.size(1)):
            predictions, dec_hidden, _ = decoder(dec_input.to(device),
                                                 dec_hidden.to(device),
                                                 enc_output.to(device))
            loss += loss_function(ys[:, t].to(device), predictions.to(device))
            predictions = predictions.squeeze(0)
            final_sentences[:, t] = predictions.argmax(axis=1)

            dec_input = ys[:, t].unsqueeze(1)

        target_sentences.extend(convert_to_sentences(
            ys, word_index=target_word_index))
        translated_sentences.extend(convert_to_sentences(
            final_sentences, word_index=target_word_index))
        input_sentences.extend(convert_to_sentences(
            Xs.numpy().T, word_index=input_word_index))

        batch_loss = (loss / int(ys.size(1)))
        total_loss += batch_loss

    total_loss = total_loss / num_batches
    return dict(
        batch_loss=batch_loss,
        total_loss=total_loss,
        input_sentences=input_sentences,
        target_sentences=target_sentences,
        predicted_sentences=translated_sentences)


def print_results(label='', num_batches=10, results=None):
    print()
    print('=' * 40)
    if results is None:
        results = predict_batch(num_batches=num_batches)
    print('==== Translations of Training Set Examples ====')
    triplets = list(zip(results['input_sentences'],
                        results['predicted_sentences'], results['target_sentences']))
    for triplet in triplets:
        print(triplet[0] + f'\t<-input ({source_lang_name})')
        print()
        print(triplet[2] + f'\t<-truth ({target_lang_name})')
        print(triplet[1] + f'\t<-prediction ({target_lang_name})')
        print()
    print(
        f'==== (total_loss: {results["total_loss"]} ====')
    print('=' * 40)
    print()
    return pd.DataFrame(triplets, columns=[
        f'{source_lang_name}',
        f'predicted_{target_lang_name}',
        f'truth_{target_lang_name}'])


def average_rouge_n(pairs):
    """ Compute the average rouge_n score for a sequence of 2-tuples of nautral language strings """
    pairs = list(pairs)
    ave = 0
    n = len(pairs)
    for pair in pairs:
        ave += rouge_n(*pair) / n
    return ave


if __name__ == '__main__':
    # TODO: Combine the encoder and decoder into one class
    # calculate the max_length of input and output tensor
    try:
        NUM_EXAMPLES = int(os.environ['NUM_EXAMPLES'])
    except Exception:
        pass
    df = load_dataset(NUM_EXAMPLES)

    # Split training and val set and remove overlapping sentences from val
    train_df, val_df = custom_train_test_split(df, source_lang_name, target_lang_name)

    # Create token dictionary using train_df
    input_word_index = LanguageIndex(phrases=train_df[source_lang_name].values, name=source_lang_name)
    target_word_index = LanguageIndex(phrases=train_df[target_lang_name].values, name=target_lang_name)

    assert input_word_index.get('aldkjlakjl') == input_word_index.get('<oov>')
    assert target_word_index.get('aldkjlakjl') == target_word_index.get('<oov>')
    print(input_word_index.get('<oov>'))
    print(target_word_index.get('<oov>'))

    # Convert list of words to tensors of tokens
    input_train_tensors = texts_to_tensors(texts=train_df[source_lang_name], language_index=input_word_index)
#    print("input train tensors",input_train_tensors[:20])
    target_train_tensors = texts_to_tensors(texts=train_df[target_lang_name], language_index=target_word_index)
#    print("target train tensors",target_train_tensors[:20])

    input_val_tensors = texts_to_tensors(texts=val_df[source_lang_name], language_index=input_word_index)
#    print("input val tensors",input_val_tensors[:20])
    target_val_tensors = texts_to_tensors(texts=val_df[target_lang_name], language_index=target_word_index)

    # calculate the max_length of input and output tensor
    max_length_inp_train = max(len(t) for t in input_train_tensors)
    max_length_inp_val = max(len(t) for t in input_val_tensors)
    max_length_inp = max(max_length_inp_train, max_length_inp_val)
    max_length_tar_train = max(len(t) for t in target_train_tensors)
    max_length_tar_val = max(len(t) for t in target_val_tensors)
    max_length_tar = max(max_length_tar_train, max_length_tar_val)

    # <pad> token
    input_train_tensors = [pad_token_id_list(x, max_length_inp, input_word_index[PAD_TOKEN]) for x in input_train_tensors]
    target_train_tensors = [pad_token_id_list(x, max_length_tar, target_word_index[PAD_TOKEN]) for x in target_train_tensors]
    input_val_tensors = [pad_token_id_list(x, max_length_inp, input_word_index[PAD_TOKEN]) for x in input_val_tensors]
    target_val_tensors = [pad_token_id_list(x, max_length_tar, target_word_index[PAD_TOKEN]) for x in target_val_tensors]

    input_train_tensors = [pad_token_id_list(
        x, max_length_inp, input_word_index[PAD_TOKEN]) for x in input_train_tensors]
    target_train_tensors = [pad_token_id_list(
        x, max_length_tar, target_word_index[PAD_TOKEN]) for x in target_train_tensors]
    # print(f"len(target_tensors): {len(target_tensors)}")

    # Creating training and validation sets using an 80-20 split
    # input_tensor_train, input_tensor_val, target_tensor_train, target_tensor_val = train_test_split(
    #     input_tensor, target_tensor, test_size=0.05, seed=101)
    train_dataset = TranslationDataset(input_train_tensors, target_train_tensors)
    val_dataset = TranslationDataset(input_val_tensors, target_val_tensors)

    BUFFER_SIZE = len(input_train_tensors)
    N_BATCH = BUFFER_SIZE // BATCH_SIZE

    train_data = DataLoader(train_dataset,
                            batch_size=BATCH_SIZE,
                            drop_last=True,
                            shuffle=True)

    val_data = DataLoader(val_dataset,
                          batch_size=BATCH_SIZE,
                          drop_last=True,
                          shuffle=True)

    encoder = Encoder(
        len(input_word_index),
        embedding_dim=EMBEDDING_DIM,
        layer_enc_units=LAYER_NUM_UNITS,
        batch_size=BATCH_SIZE)
    decoder = Decoder(
        len(target_word_index),
        embedding_dim=EMBEDDING_DIM,
        layer_enc_units=LAYER_NUM_UNITS,
        dec_units=LAYER_NUM_UNITS[-1],
        batch_size=BATCH_SIZE)

    encoder.to(DEVICE)
    decoder.to(DEVICE)

    optimizer = optim.Adam(list(encoder.parameters()) +
                           list(decoder.parameters()), lr=LEARNING_RATE)

    encoder, decoder = train(encoder=encoder, decoder=decoder, optimizer=optimizer, num_epochs=NUM_EPOCHS, dataset=train_data)

    # FIXME: predict_batch on val set fails if NUM_EXAMPLES is 1000 and
    train_results = predict_batch(
        dataset=train_data,
        source_word_index=input_word_index,
        target_word_index=target_word_index,
        num_batches=10)
    triplets = print_results(label='train', results=train_results)
    print("Rouge score", rouge_n(triplets['predicted_english'][0], triplets['truth_english'][0]))

    rouge_n_score = average_rouge_n(zip(triplets['predicted_english'], triplets['truth_english']))
    print()
    print(rouge_n_score)
